var gulp 	  = require('gulp'),
    sass      = require('gulp-sass'),
    cleanCSS  = require('gulp-clean-css'),
    concat    = require('gulp-concat'),
    uglify   = require('gulp-uglify'),
    assets    = { src: 'src/', dist: 'dist/' };

var paths    = {
    scss: {
        src: assets.src + 'scss/'
    },
    img: {
        src: assets.src + 'img/'
    },
    font: {
        src: assets.src + 'font/'
    },
    js: {
        src:  assets.src + 'js/'
    }
}

gulp.task('sass', function() {
	return gulp.src(paths.scss.src+'main.scss')
    .pipe(sass({
        includePaths: ['node_modules'],
        outputStyle: 'compressed'
    }))
    .pipe(concat('/styles.min.css'))
    .pipe(gulp.dest(assets.dist));
});


gulp.task('script', function() {
	return gulp.src([paths.js.src + 'vendor/*.js', paths.js.src + '*.js'])
    // .pipe(uglify())
    .pipe(concat('script.min.js'))
    .pipe(gulp.dest(assets.dist));
});


/**
 * Execution Tasks
 */
gulp.task( 'default', [ 'styles','scripts'] );
gulp.task( 'scripts', [ 'script'] );
gulp.task( 'styles', [ 'sass'] );