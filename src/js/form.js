// form validation
jQuery('.validation-form').validate({
	// errorClass: 'text-error',
	rules: {
		email: {
			required: true,
			email: true
		},
		password: {
			required: true
		}
	},
	messages: {
		email: {
			required: "Campo obrigatório",
			email: "Digite um e-mail válido"
		},
		password: {
			required: "Campo obrigatório"
		}
	},

	submitHandler: function (form) {
		
	}
});