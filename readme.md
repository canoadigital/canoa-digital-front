# Canoa Digital Front-end Starter

Our front-end library to start new projects based on Bootstrap 4. With focus in organize and streamline the development process.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development.

### Prerequisites

What things you need to install the software and how to install them

```
NPM
GULP
```

### Installing

After installing NPM, update NPM

```
npm update
```

And then install the dependencies

```
npm install
```

## Running and Compile

Compile gulp (css, javascript and compress)

```
gulp
```

## Deployment

For all new package release, we need to change the version on the package.json file. Example:

```
{
"version": "0.1.2"
}
```

### After that, you need create an account or login to npm with your username, password and email.


And then publish to npm.

```
npm publish --access=public
```

## Downloading

To download the library as a npm package, run:

```
npm install canoa-digital-front --save-dev
```

## Built With

* [ Bootstrap 4 ](https://getbootstrap.com/) - The most popular HTML, CSS, and JS library in the world.

## Authors

* **Thomas Nunes** - *Initial work*

## License

This project is licensed under the MIT License